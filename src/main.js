import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
// import RSA from "./assets/js/index.js"

import axios from "axios"
import  "js-base64";

// import "bootstrap";
// import "bootstrap/dist/css/bootstrap.min.css";
// import "./assets/css/style.css";
import "./assets/style.css"
Vue.config.productionTip = false;

new Vue({
  router,
  // RSA,
  axios,
  render: function(h) {
    return h(App);
  },
}).$mount("#app");
