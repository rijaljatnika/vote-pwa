import Vue from "vue";
import VueRouter from "vue-router";
import Cookies from "js-cookie";
import Home from "../views/Home.vue";
import loginPemilih from "../views/loginPemilih.vue"
import info from "../views/viewInfo.vue"
Vue.use(VueRouter);

const routes = [
  {
    path: "/admin",
    name: "Admin",
    component: Home,
  },
  {
    path: "/pemilih-tetap",
    name: "pemilihTetap",
    component: function() {
      return import("../views/viewPemilihTetap.vue");
    }
  },
  {
    path: "/hasil-pemilihan",
    name: "hasilPemilihan",
    component: function() {
      return import("../views/viewHasilPemilihan.vue");
    }
  },

  // UNTUK PEMILIH SAJA!
  {
    path: "/",
    name: "Login Pemilih",
    component: loginPemilih, alias: '/login'
  },
  {
    path: "/info",
    name: "Info",
    component: info,
    meta: { requiresAuth: true }
  },
  {
    path: "/pemilihan",
    name: "viewPemilihan",
    component: function() {
      return import("../views/viewPemilihan.vue");
    },
    meta: { requiresAuth: true }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!Cookies.get('token')) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

export default router;
